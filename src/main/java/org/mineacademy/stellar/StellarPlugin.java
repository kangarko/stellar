package org.mineacademy.stellar;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.plugin.SimplePlugin;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.stellar.command.StellarCommand;
import org.mineacademy.stellar.listener.PlayerListener;

/**
 * This is the main class of your plugin that is found by the "main" key
 * in your plugin.yml (we define this in our pom.xml) and loaded by Bukkit.
 *
 * By default this class already implements event listener so you can just
 * put your methods listening for events here without any more action needed!
 */
public final class StellarPlugin extends SimplePlugin {

	/**
	 * The method called automatically when your plugin starts,
	 * such as when the server is starting or the plugin is being reloaded.
	 */
	@Override
	protected void onPluginStart() {

		// Use this method to register commands that extend the SimpleCommand class
		registerCommand(new StellarCommand());

		// Use this method to register classes that listen for events and implement the org.bukkit.event.Listener class.
		registerEvents(new PlayerListener());
	}

	/**
	 * The method called automatically when your plugin stops,
	 * such as when the server is stopping or the plugin is being reloaded.
	 */
	@Override
	protected void onPluginStop() {

	}

	/**
	 * This method listens for the {@link PlayerJoinEvent} event and runs
	 * the code within it automatically on player join.
	 *
	 * How? Bukkit will find classes inside of your plugin that implement Listener
	 * (this main class does it automatically) and then looks for methods that have
	 * the @EventHandler annotation and a single parameter of the Event class.
	 *
	 * The names "onPlayerJoin" and "event" do not matter, what matters is the @EventHandler
	 * and the single parameter PlayerJoinEvent. Do not add more parameters otherwise it won't work!
	 *
	 * @param event
	 */
	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent event) {

		// Get the player who just joined
		final Player player = event.getPlayer();

		player.sendMessage("Welcome to the server, " + player.getName() + "!");

		player.getInventory().addItem(
				ItemCreator
						.of(CompMaterial.DIAMOND, "&4Red Diamond", "", "This is a special diamond!")
						.build().makeSurvival()
		);
	}
}
