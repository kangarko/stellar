package org.mineacademy.stellar.command;

import org.mineacademy.fo.command.SimpleCommand;

/**
 * This is a command class that executes the /stellar (or /st) command.
 *
 * All of your command classes should extend SimpleCommand
 */
public final class StellarCommand extends SimpleCommand {

	/**
	 * Create a new instance of this class and pass the command name
	 * to the parent's constructor.
	 *
	 * The first name, stellar, is the main command name, called the label.
	 * Everything after split by | is called a command alias, which means that if
	 * we run /st we will run this command just as we would by called /stellar.
	 *
	 * You can use multiple aliases and split them by | such as stellar|st|stell|stlr
	 */
	public StellarCommand() {
		super("stellar|st");
	}

	/**
	 * The main method called automatically when someone does /stellar or /st according
	 * to the name and aliases above
	 */
	@Override
	protected void onCommand() {
		// You have access to "sender" and "args" variables directly

	}
}
