package org.mineacademy.stellar.listener;

import org.bukkit.event.Listener;

/**
 * This class implements the Listener, which means that Bukkit
 * will automatically scan it for certain methods and run them
 * on certain events (see StellarPlugin for more information).
 */
public final class PlayerListener implements Listener {

}
